//
//  Form.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation
import ObjectMapper

class Form: NSObject, ImmutableMappable {
    let title: String
    let fields: [Field]
    
    required init(map: Map) throws {
        self.title  = try map.value("title")
        self.fields = try map.value("fields")
    }
}
