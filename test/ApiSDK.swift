//
//  ApiSDK.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation


class ApiSdk{
    
    static let shared:ApiSdk = ApiSdk()
    
    typealias CompletionOnSuccess = (_ json: Any) -> ()
    typealias CompletionOnFaild   = (_ json: Any?, _ error: Error?) -> ()
    
    
    ///Get meta form json
    func getMetaInfo(completionOnSuccess:@escaping (_ form:Form)->(),
                     completionOnFaild:@escaping CompletionOnFaild) -> URLSessionTask?{
        
        return self._apiRequest(
            path: self._urlHelper.mataInfoPath,
            method: .post,
            completionOnSuccess: { json in
                do{
                    let form = try Form(JSONObject: json)
                    completionOnSuccess(form)
                }catch let error{
                    completionOnFaild(json, error)
                }
        },
            completionOnFaild: completionOnFaild
        )
    }
    
    func sendData(params:[String: Any] = [:], completionOnSuccess:@escaping (_ form:ResultMessage)->(),
                  completionOnFaild:@escaping CompletionOnFaild) -> URLSessionTask?{
        
        return self._apiRequest(
            path: self._urlHelper.dataPath,
            method: .post,
            params: params,
            completionOnSuccess: { json in
                do{
                    let message = try ResultMessage(JSONObject: json)
                    completionOnSuccess(message)
                }catch let error{
                    completionOnFaild(json, error)
                }
        },
            completionOnFaild: completionOnFaild
        )
    }
    
    //******************************************************************************************************************
    //PRIVATE ZONE -----------------------------------------------------------------------------------------------------
    //******************************************************************************************************************
    private var _urlHelper: ApiUrlHelper
    
    
    private init(){
        self._urlHelper = ApiUrlHelper()
    }
    
    ///requst to api
    private func _apiRequest(path: String, method: HTTPMethod, headers: [String: String] = [:], params:[String: Any] = [:],
                             completionOnSuccess:@escaping CompletionOnSuccess, completionOnFaild:@escaping CompletionOnFaild) -> URLSessionTask? {
        
        guard let url = URL(string: path) else{
            
            let error = NSError(
                domain: path,
                code: 5051,
                userInfo: ["error": "wrong Url"]
            )
            
            completionOnFaild(nil, error)
            
            return nil
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod           = method.rawValue
        urlRequest.timeoutInterval      = 20
        urlRequest.allHTTPHeaderFields  = headers
        
        do{
            let data = try JSONSerialization.data(withJSONObject: params, options: [])
            urlRequest.httpBody = data
        }catch let error{
            completionOnFaild(params, error)
            return nil
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            let json: Any
            do{
                if let data = data{
                    json = try JSONSerialization.jsonObject(with: data, options: [])
                }else{
                    throw NSError(
                        domain: path,
                        code: 5051,
                        userInfo: ["error": "data is empty"]
                    )
                }
            }catch let error{
                completionOnFaild(nil, error)
                return
            }
            
            if let error = error{
                completionOnFaild(json, error)
                return
            }
            
            completionOnSuccess(json)
        }
        
        task.resume()
        
        return task
    }
}
