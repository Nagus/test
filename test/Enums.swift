//
//  Enums.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation

enum HTTPMethod: String{
    case post = "POST"
}

enum TypeField: String{
    case text   = "TEXT"
    case number = "NUMERIC"
    case list   = "LIST"
}
