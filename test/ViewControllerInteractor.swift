//
//  ViewControllerInteractor.swift
//  test
//
//  Created by Алексей Серёжин on 20.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation

class ViewControllerInteractor{
    
    let sdk =  ApiSdk.shared
    
    var form: Form?
    var message: ResultMessage?
    var currentTask: URLSessionTask?
    
    func getForm(completionOnSuccess:@escaping ()->(), completionOnFaild:@escaping ()->()){
        self.currentTask = self.sdk.getMetaInfo(
            completionOnSuccess: { form in
                self.form = form
                completionOnSuccess()
        },
            completionOnFaild: { (json, error) in
                StaticErrorHelper.printErrorString(
                    file: #file,
                    function: #function,
                    line: #line,
                    error: error,
                    json: json
                )
                
                self.form = nil
                
                completionOnFaild()
        })
    }
    
    func sendData(completionOnSuccess:@escaping ()->(), completionOnFaild:@escaping ()->()){
        
        guard let fields = self.form?.fields else{
            
            completionOnFaild()
            
            StaticErrorHelper.printErrorString(
                file: #file,
                function: #function,
                line: #line
            )
            
            return
        }
        
        var params: [String: Any] = [:]
        
        for field in fields{
            params[field.name] = field.inputValue
        }
        
        self.currentTask = self.sdk.sendData(
            params: ["form":params],
            completionOnSuccess: { message in
                self.message = message
                completionOnSuccess()
        },
            completionOnFaild: { (json, error) in
                StaticErrorHelper.printErrorString(
                    file: #file,
                    function: #function,
                    line: #line,
                    error: error,
                    json: json
                )
                
                self.message = nil
                
                completionOnFaild()
        })
    }
}
