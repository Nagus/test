//
//  InputPickerView.swift
//  test
//
//  Created by Алексей Серёжин on 23.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit

class InputPickerView: UIView {
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var inputPickerViewInteractor: InputPickerViewInteractorProtocol?
}

extension InputPickerView: UIPickerViewDelegate{
    /*
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat{
        return self.frame.width
    }
    */
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if row < self.inputPickerViewInteractor?.titles.count{
            return self.inputPickerViewInteractor?.titles[row].value
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.inputPickerViewInteractor?.select(index: row)
    }
}

extension InputPickerView: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.inputPickerViewInteractor?.titles.count ?? 0
    }
}
