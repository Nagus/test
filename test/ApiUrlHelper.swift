//
//  ApiUrlHelper.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation

class ApiUrlHelper{
    
    var root: String
    
    init(root:String = "http://test.clevertec.ru/tt/"){
        self.root = root
    }
    
    var mataInfoPath: String{
        return self.root + "meta"
    }
    
    var dataPath: String{
        return self.root + "data"
    }
}


