//
//  InputPickerViewInteractor.swift
//  test
//
//  Created by Алексей Серёжин on 23.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation

protocol InputPickerViewInteractorProtocol{
    var titles: [(key: String, value: String)] {get}
    func select(index: Int)
}
