//
//  ContraintsView.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit

extension UIView{
    func addBorderConstraintToView(_ view:UIView, top:CGFloat? = nil, bottom:CGFloat? = nil, leading:CGFloat? = nil, trailing:CGFloat? = nil){
        if let leading = leading {
            let leadingConstraint = NSLayoutConstraint(
                item: view,
                attribute: .leading,
                relatedBy: .equal,
                toItem: self,
                attribute: .leading,
                multiplier: 1,
                constant: leading
            )
            leadingConstraint.identifier = "leadingConstraint"
            self.addConstraint(leadingConstraint)
        }
        
        if let trailing = trailing {
            let trailingConstraint = NSLayoutConstraint(
                item: view,
                attribute: .trailing,
                relatedBy: .equal,
                toItem: self,
                attribute: .trailing,
                multiplier: 1,
                constant: trailing
            )
            trailingConstraint.identifier = "trailingConstraint"
            self.addConstraint(trailingConstraint)
        }
        
        if let top = top {
            let topConstraint = NSLayoutConstraint(
                item: view,
                attribute: .top,
                relatedBy: .equal,
                toItem: self,
                attribute: .top,
                multiplier: 1,
                constant: top
            )
            topConstraint.identifier = "topConstraint"
            self.addConstraint(topConstraint)
        }
        
        if let bottom = bottom {
            let bottomConstraint = NSLayoutConstraint(
                item: view,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: self,
                attribute: .bottom,
                multiplier: 1,
                constant: bottom
            )
            bottomConstraint.identifier = "bottomConstraint"
            self.addConstraint(bottomConstraint)
        }
    }
    
    func addSpacingConstraintFromViewToView(fromView:Any, toView:Any, fromAttribute:NSLayoutAttribute,
                                            toAttribute:NSLayoutAttribute, constant:CGFloat = 0, multiplier:CGFloat = 1){
        let spacingConstraint = NSLayoutConstraint(
            item: fromView,
            attribute: fromAttribute,
            relatedBy: .equal,
            toItem: toView,
            attribute: toAttribute,
            multiplier: multiplier,
            constant: constant
        )
        spacingConstraint.identifier = "spacingConstraint"
        self.addConstraint(spacingConstraint)
    }
    
    func addSizeConstraints(height:CGFloat? = nil, heightAttribute:NSLayoutRelation = .equal, width: CGFloat? = nil, widthAttribute:NSLayoutRelation = .equal){
        
        if let height = height{
            let heightConstraint = NSLayoutConstraint(
                item: self,
                attribute: .height,
                relatedBy: heightAttribute,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1,
                constant: height
            )
            heightConstraint.identifier = "heightConstraintLine"
            self.addConstraint(heightConstraint)
        }
        
        if let width = width{
            let widthConstraint = NSLayoutConstraint(
                item: self,
                attribute: .width,
                relatedBy: widthAttribute,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1,
                constant: width
            )
            widthConstraint.identifier = "widthConstraint"
            self.addConstraint(widthConstraint)
        }
    }
    
    func addCenterConstraintsToView(_ view:UIView, deltaX:CGFloat? = nil, deltaY: CGFloat? = nil){
        if let deltaX = deltaX{
            let centerXConstraint = NSLayoutConstraint(
                item: view,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: self,
                attribute: .centerX,
                multiplier: 1,
                constant: deltaX
            )
            centerXConstraint.identifier = "centerXConstraint"
            self.addConstraint(centerXConstraint)
        }
        
        if let deltaY = deltaY{
            let centerYConstraint = NSLayoutConstraint(
                item: view,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: self,
                attribute: .centerY,
                multiplier: 1,
                constant: deltaY
            )
            centerYConstraint.identifier = "centerYConstraint"
            self.addConstraint(centerYConstraint)
        }
    }
    
    func addRatioConstraint(multiplier: CGFloat){
        let ratioConstraint = NSLayoutConstraint(
            item: self,
            attribute: .height,
            relatedBy: .equal,
            toItem: self,
            attribute: .width,
            multiplier: multiplier,
            constant: 0
        )
        ratioConstraint.identifier = "ratioConstraint"
        self.addConstraint(ratioConstraint)
    }
}
