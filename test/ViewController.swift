//
//  ViewController.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let cellIdentifier = "TableViewCell"
    
    weak var tableView: UITableView!
    weak var bottomTVConstraint: NSLayoutConstraint!
    
    let activityIndicator = Bundle.main.loadNibNamed("CustomActivityIndicator", owner: self, options: nil)?.first as! CustomActivityIndicator
    
    let interactor: ViewControllerInteractor = ViewControllerInteractor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.registerForKeyboardNotifications()
        self.setTableView()
        self.setActivityIndicator()
        self.startIndicatorAnimating()
        
        self.interactor.getForm(completionOnSuccess: {
            
            OperationQueue.main.addOperation {
                self.title = self.interactor.form?.title
                self.tableView.reloadData()
            }
            
            self.stopIndicatorAnimating()
        }, completionOnFaild: {
            self.stopIndicatorAnimating()
        })
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func startIndicatorAnimating(){
        OperationQueue.main.addOperation {
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.frame = self.view.bounds
        }
    }
    
    func stopIndicatorAnimating(){
        OperationQueue.main.addOperation {
            self.activityIndicator.removeFromSuperview()
        }
    }
    
    func setActivityIndicator(){
        self.activityIndicator.cancelButton.addTarget(self, action: #selector(ViewController.touchCancelButton(_:)), for: .touchUpInside)
    }
    
    func touchCancelButton(_ sender: UIButton){
        self.interactor.currentTask?.cancel()
    }
    
    func setTableView(){
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate                                  = self
        tableView.dataSource                                = self
        tableView.separatorStyle                            = .none
        
        tableView.register(UINib(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        
        self.view.addSubview(tableView)
        self.view.addBorderConstraintToView(tableView, top: 0, leading: 0, trailing: 0)
        
        self.bottomTVConstraint =  NSLayoutConstraint(
            item: self.view,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: tableView,
            attribute: .bottom,
            multiplier: 1,
            constant: 0
        )
        
        self.view.addConstraint(self.bottomTVConstraint)
        
        self.tableView = tableView
    }
    
    func registerForKeyboardNotifications(){
        
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.keyboardWasShown(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.keyboardWillBeHidden(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    func keyboardWasShown(_ notification: Notification){
        guard let userInfo = notification.userInfo else{
            return
        }
        
        guard let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else{
            return
        }
        
        self.bottomTVConstraint.constant = keyboardSize.height
    }
    
    func keyboardWillBeHidden(_ notification: Notification){
        self.bottomTVConstraint.constant = 0
    }
    
    func showAlert(title: String? = nil, messege: String, canseltitle: String, handler: ((UIAlertAction) -> Void)? = nil){
        let errorAlert = UIAlertController(title: title, message: messege, preferredStyle: .alert)
        let canselAction = UIAlertAction(title: canseltitle, style: .cancel, handler: handler)
        
        errorAlert.addAction(canselAction)
        OperationQueue.main.addOperation({
            self.present(errorAlert, animated: true, completion: nil)
        })
    }
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        if let buttonCell = cell as? ButtonCell {
            
            buttonCell.button.addTarget(self, action: #selector(ViewController.touchUpInsideButton(_:)), for: .touchUpInside)
            
            return
        }
        
        guard let tableViewCell = cell as? TableViewCell else {
            return
        }
        
        guard indexPath.row < self.interactor.form?.fields.count, let field = self.interactor.form?.fields[indexPath.row] else{
            return
        }
        
        tableViewCell.setTableViewCell(content: field)
        tableViewCell.textField.isUserInteractionEnabled = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func touchUpInsideButton(_ sender: UIButton){
        
        self.dismissKeyboard()
        
        self.interactor.sendData(
            completionOnSuccess: {
                
                self.stopIndicatorAnimating()
                
                let message = self.interactor.message
                
                self.showAlert(messege: message?.result ?? "Error", canseltitle: "Ok")                
        },
            completionOnFaild: {
                
                self.stopIndicatorAnimating()
        })
    }
}

extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section {
        case 0:
            return self.interactor.form?.fields.count ?? 0
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 1 {
            return tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if self.interactor.form == nil{
            return 0
        }else{
            return 2
        }
    }
}

