//
//  TableViewCell.swift
//  test
//
//  Created by Алексей Серёжин on 20.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit
import APNumberPad

class TableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var content: Field?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.text        = nil
        self.textField.text         = nil
        self.textField.keyboardType = .default
        self.textField.inputView    = nil
        self.textField.delegate     = self
        self.content                = nil
    }
    
    func setTableViewCell(content: Field){
        self.titleLabel.text = content.title
        self.setTextView(content:content)
    }
    
    func setTextView(content: Field){
        
        self.content = content
        
        switch content.type{
        case .text:
            self.textField.keyboardType = .default
            self.textField.inputView    = nil
        case .number:
            
            self.textField.inputView = {
                
                let numberPad = APNumberPad(delegate: self)
                numberPad.leftFunctionButton.setTitle(".", for: .normal)
                return numberPad
            }()
        case .list:
            
            self.textField.text = content.values?.first?.value
            self.textField.inputView = {
                
                let inputView = Bundle.main.loadNibNamed("InputPickerView", owner: self, options: nil)?.first as! InputPickerView
                inputView.inputPickerViewInteractor = self
                return inputView
            }()
        }
    }
}

extension TableViewCell:APNumberPadDelegate{
    func numberPad(_ numberPad: APNumberPad, functionButtonAction functionButton: UIButton, textInput: UIResponder){
        guard let textInput = textInput as? UITextInput else {
            return
        }
        
        if let text = self.textField.text, !text.contains(".") && !text.isEmpty{
            textInput.insertText(".")
        }
    }
}

extension TableViewCell: InputPickerViewInteractorProtocol{
    var titles: [(key: String, value: String)]{
        return self.content?.values ?? []
    }
    
    func select(index: Int){
        guard index >= 0 && index < self.content?.values?.count else{
            return
        }
        
        self.textField.text = self.content?.values?[index].value
        self.content?.inputValue = self.content?.values?[index].value ?? ""
    }
}

extension TableViewCell: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if self.content?.type == .number{
            
            let newCharacters = CharacterSet(charactersIn: string)
            return CharacterSet.decimalDigits.isSuperset(of: newCharacters)
        }else if self.content?.type == .list{
            return false
        }
        
        return true
    }
    
    
    //set value from textView
    func textFieldDidEndEditing(_ textField: UITextField){
        if self.content?.type == .list{
            
            guard let values =  self.content?.values else {
                return
            }
            
            for (key, value) in values{
                if value == textField.text{
                    self.content?.inputValue = key
                    break
                }
            }
        }else if self.content?.type == .number {
            
            if textField.text?.characters.last == "."{
                textField.text?.characters.removeLast()
            }
            
            self.content?.inputValue = textField.text ?? ""
        }else{
            
            self.content?.inputValue = textField.text ?? ""
        }
    }
}

