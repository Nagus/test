//
//  ButtonCell.swift
//  test
//
//  Created by Алексей Серёжин on 23.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    @IBOutlet weak var button: UIButton!
}
