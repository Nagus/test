//
//  Common.swift
//  test
//
//  Created by Алексей Серёжин on 21.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation


func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}
