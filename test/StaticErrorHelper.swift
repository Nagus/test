//
//  StaticErrorHelper.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import UIKit

class StaticErrorHelper{
    
    ///create error on point in file
    class func createErrorString(file: StaticString? = nil, function: StaticString? = nil, line: Int? = nil, error: Error? = nil, json: Any? = nil) -> String{
        let file = file ?? "UnknownFile"
        let fileName = (String(describing: file).components(separatedBy: "/").last ?? "").components(separatedBy: ".").first ?? "UnknownFile"
        var errorString = "\n\(fileName).\(function ?? "UnknownFunction"):\(line ?? 0)"
        
        if let err = error{
            errorString += "\nError: \(err)"
        }
        
        if let js = json{
            errorString += "\njson: \(js)"
        }
        
        return errorString
    }
    
    ///print error on point in file
    class func printErrorString(file: StaticString? = nil, function: StaticString? = nil, line: Int? = nil, error: Error? = nil, json: Any? = nil){
        print(createErrorString(file: file, function: function, line: line, error: error, json: json))
    }
    
    ///print error allert
    class func alertError(_ viewController: UIViewController, messege: String, canseltitle: String, handler: ((UIAlertAction) -> Void)? = nil){
        let errorAlert = UIAlertController(title: "Error", message: messege, preferredStyle: .alert)
        let canselAction = UIAlertAction(title: canseltitle, style: .cancel, handler: handler)
        
        errorAlert.addAction(canselAction)
        OperationQueue.main.addOperation({
            viewController.present(errorAlert, animated: true, completion: nil)
        })
    }
    
}
