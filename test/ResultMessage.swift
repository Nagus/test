//
//  ResultMessage.swift
//  test
//
//  Created by Алексей Серёжин on 22.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultMessage: NSObject, ImmutableMappable {
    let result: String
    
    required init(map: Map) throws {
        self.result = try map.value("result")
    }
}
