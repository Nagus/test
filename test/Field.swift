//
//  Field.swift
//  test
//
//  Created by Алексей Серёжин on 19.03.17.
//  Copyright © 2017 NMoiseev. All rights reserved.
//

import Foundation
import ObjectMapper

class Field: NSObject,ImmutableMappable{
    //name for request
    let name: String
    //title for showing
    let title: String
    //type field
    let type: TypeField
    var values: [(key:String, value:String)]?
    
    var inputValue: String
    
    required init(map: Map) throws {
        self.name       = try map.value("name")
        self.title      = try map.value("title")
        self.type       = try map.value("type")
        self.inputValue = ""
        
        guard let values: [String:String] = try? map.value("values") else{
            return
        }
    
        self.values = values.sorted(by: { (element0, element1) -> Bool in
            return element0.key < element1.key
        })
    }
}
